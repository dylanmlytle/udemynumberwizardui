﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewRound : MonoBehaviour {

    int min;
    int max;
    int currentGuess;
    int roundCount;
    bool isGameFinished;

    int maxGuessesAllowed = 11;

    public Text guessText;
    public Text titleText;


    // Use this for initialization
    void Start () {
        this.max = 1000;
        this.min = 1;
        this.roundCount = 0;

        SetUpNewRound();
    }

    public void HigherClicked() {
        this.min = currentGuess;
        SetUpNewRound();
    }

    public void LowerClicked() {
        this.max = currentGuess;
        SetUpNewRound();
    }

    public void CorrectClicked() {
        SceneManager.LoadScene("Lose");
    }

    // Update is called once per frame
    void Update () {

    }

    void SetUpNewRound() {
        if(this.roundCount >= maxGuessesAllowed) {
            SceneManager.LoadScene("Win");
        } else {
            this.roundCount++;
            titleText.text = "Round " + this.roundCount.ToString();
            UpdateCurrentGuess();
        }
    }

    void UpdateCurrentGuess()
    {
        this.currentGuess = Random.Range(this.min, this.max + 1);
        this.guessText.text = this.currentGuess.ToString() + "?";
    }
}
